# Example Frontend Automation using Protractor - Jasmine Framework:
#### Author: 
Paul Joseph Farrell
#### Email: 
pauljosephfarrell89@gmail.com
#### LinkedIn: 
[Feel free to add me on Linked In](https://www.linkedin.com/in/faz540/)
#### The Objective Of This Repository:
The aim of this repository is to act as a tutorial for fellow testers (mostly for testers in the 'Global Tech Quality SE 2' WhatsApp group) that want to start writing frontend automation.
The second aim is to act as a 'portfolio' to demonstrate how I write frontend automation for any future employers. 
#### Quick Bio:  
I started my testing career in 2012 where I was lucky enough to be a Functionality Tester for Sony PlayStation for over 2 years.

In 2014, I joined a small IT company known as InventiveIT, which later grew signficantly and became ADVAM UK.
It was at Inventive IT/ ADVAM UK where I learnt most of my skills.(Cross browser and device testing, API testing using Postman and I dabbled a little in the world of frontend automation using Ruby.

I had a great career at InventiveIT/ADVAM UK but decided to take a risk and leave my comfort zone and challenge myself by joining Beauty Bay, an eCommerce company specialising in cosmetics, in 2016.

At BeautyBay, I'm able to focus a lot more on web automation using Javascript(API/Frontend).


## Prerequisite/ Installation:
To clone this repository, you'll obviously need [Git](https://git-scm.com/download/win)
The above assumes you're using Windows...

You must have [Node.js](https://www.nodejs.org/) installed.

You must also have the [Java Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/index.html) installed in order to run the local Selenium Server.

This test will run against Google Chrome, so make sure you [download the latest version of the Chrome browser.](https://www.google.co.in/chrome/index.html)

## Cloning The Repository:
Open 'Command Prompt' in Windows and navigate to where you want to download the repository.
Assuming your default location is 'C:\Users\{Your_Name}'
```
cd Desktop
```
Now you're looking at your desktop, clone the repository and it'll be placed inside a folder called 'protractor-jasmine'
```
git clone https://Faz540@bitbucket.org/Faz540/protractor-jasmine.git
```
Now, navigate to the 'protractor-jasmine' folder you just cloned from BitBucket.
```
cd protractor-jasmine
```
## Install Dependencies:
First, you'll need to globally install Protractor on your computer using Node.
```
npm install -g protractor
```
Secondly, install the dependencies in the repo you have just cloned.
```
npm install
```
Thirdly, install some more Protractor dependencies which allow you to run automated tests against Chrome etc.
```
webdriver-manager update
```
Lastly, run the Selenium Server locally on your machine using the below command and leave that window open:
```
webdriver-manager start
```
## When running your tests, open another command window as you want to keep the previous one open that is running the local Selenium Server.
## To run all test files inside 'tests' folder, run the following command:
```
npm test
```
When running the tests for the first time you may be asked by Windows if Java can access your network.
Click 'Allow Access'. 
## To run a specific test file against (Only the Homepage tests):
```
npm run test:home
```
## To run only the Product Page tests:
```
npm run test:product
```