//const baseUrl = process.env.BASE_URL || 'http://automationpractice.com/index.php';

const SpecReporter = require('jasmine-spec-reporter').SpecReporter;

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [ /*"--headless", "--disable-gpu",*/ "--window-size=1400,900" ]
        }
    },
    specs: ['./tests/*.spec.js'],
    suites: {
        home: 'tests/homepage.spec.js',
        product: 'tests/product.spec.js'
    },
    framework: 'jasmine',
    jasmineNodeOpts: {
        showColors: true,
    },
    
    // Similar to a before/setup hook:
    onPrepare: function() {
        jasmine.getEnv().addReporter(new SpecReporter({
            displayFailuresSummary: true, // Displays a summary of all failures after execution.
            displayFailuredSpec: true, //  Displays a summary of all specs that failed after execution.
            displaySuiteNumber: true, // Displays the suite number executed hierarchically if the suites are set.
            displaySpecDuration: true // Displays the duration of the spec run.
        }));
        global.browser.ignoreSynchronization = true;
        console.log('About to start testing!');
    }
};