const resultsPage = require('./results.page');

const homePage = function() {
    const searchBar = element(by.id('search_query_top'));
    const searchButton = element(by.css('.button-search'));
    const banner = element(by.css('.banner'));
    
    this.open = function() {
        browser.get('http://automationpractice.com/index.php');
        browser.driver.wait(function() {
            return browser.getTitle().then(function(resolvedPageTitle) {
                return resolvedPageTitle === 'My Store';
            });
        }, 10000);
    };
    
    this.searchFor = function(searchTerm) {
        searchBar.clear();
        searchBar.sendKeys(searchTerm);
        searchButton.click();
        browser.driver.wait(function() {
            return browser.getTitle().then(function(resolvedPageTitle) {
                return resolvedPageTitle === 'Search - My Store';
            });
        }, 10000);
    };
};

module.exports = new homePage();