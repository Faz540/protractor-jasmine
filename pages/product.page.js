const wait = require('../browser-interactions/wait');

const productPage = function() {
    const addToCart = element(by.xpath('//*[@id="add_to_cart"]/button'));
    const cartPopup = element(by.id('layer_cart'));
    
    this.addItemToCart = function() {
        addToCart.click();
        browser.driver.wait(function() {
            return cartPopup.isDisplayed().then(function(elementIsDisplayed) {
                return elementIsDisplayed;
            });
        }, 10000);
    };

    this.cartPopupText = function() {
        return cartPopup.getText().then(function(resolvedText) {
            return resolvedText;
        }, 10000);
    };
};

module.exports = new productPage();