const resultsPage = function() {
    const xResultsHaveBeenFound = element(by.css('.heading-counter'));

    this.numberOfResults = function() {
        return xResultsHaveBeenFound.getText().then(function(resolvedText) {
            return parseInt(resolvedText[0]);
        }, 10000);
    };
};

module.exports = new resultsPage();