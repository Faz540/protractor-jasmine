const baseUrl = "http://automationpractice.com/index.php";
const homePage = require('../pages/home.page');
const resultsPage = require('../pages/results.page');

describe('HomePage Tests', function() {
    beforeEach(function() {
        browser.get(baseUrl);
    });
    it('returns the expected page title', function() {
        // Get the current page title and assign it to the variable 'pageTitle'
        // The below returns a 'promise':
        // The 'getTitle' function runs and returns a promise straight away.
        // As a placeholder, we've called the result of that promise 'resolvedPageTitle'
        // Once the promise has been 'resolved' (the getTitle function has completed and returned us the page title)
        // We then store the result of that promise to the 'pageTitle' variable
        pageTitle = browser.getTitle().then(function(resolvedPageTitle){
            return resolvedPageTitle;
        });
        // Expect the current page title to equal 'My Store'
        expect(pageTitle).toEqual('My Store');
    });
    it('performs a search and expects at least 1 result returned', function() {
        // Focus the search bar and clear any text that might be inside it.
        element(by.id('search_query_top')).clear();
        // Focus the search bar and enter the text 'black dresses'
        element(by.id('search_query_top')).sendKeys('black dresses');
        // Click the search button
        element(by.css('.button-search')).click();
        // Wait until the current page title is 'Search - My Store'
        // This will wait 10,000 ms (10 seconds) for the page title to equal our expected title
        // Remember, the getTitle function returns a promise.
        // So we wait for our promise to resolve with the title 'Search - My Store'
        // If the promise isn't resolved within 10 seconds, then the test will fail
        browser.driver.wait(function() {
            return browser.getTitle().then(function(resolvedPageTitle){
                return resolvedPageTitle === 'Search - My Store';
            });
        }, 10000);
        // Riiiight...lots of explanation here - bare with me!
        // We have a variable called 'results'
        // And like the getTitle function, the getText function also returns a promise
        // And like before, we want to wait for that promise to 'resolve'
        // Once the promise has been 'resolved', we chapter the first character in the returned string by using [0]
        // We then convert that string into an integer using the parseInt function
        const results = element(by.css('.heading-counter')).getText().then(function(resolvedText) {
            return parseInt(resolvedText[0]);
        }, 1000);
        // Expect the integer returned from the above function to be greater than 0.
        // This test confirms that results are returned
        expect(results).toBeGreaterThan(0);
    });
});
describe('HomePage Tests - Page Objects', function() {
    beforeEach(function() {
        homePage.open();
    });
    it('performs the above test, but this time using Page Objects', function() {
        homePage.searchFor('black dresses');
        expect(resultsPage.numberOfResults()).toBeGreaterThan(0);
    });
});