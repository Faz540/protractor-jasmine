// STOP! Please read the comments in the 'homepage.spec.js' file for an explanation on writing tests and what the intent of the test is.
// Also, read the comments in the '../pages/home.page.js' page object file as I'm sure they're beneficial too.
const baseUrl = "http://automationpractice.com/index.php";
const homePage = require('../pages/home.page');
const productPage = require('../pages/product.page');

describe('Product Page', function() {
    beforeEach(function() {
        // As this test is about the product page, I don't feel like we need to write the code going from HomePage > Product Page
        browser.get(`${baseUrl}?id_product=1&controller=product`);
    });
    it('can add a product to the user\'s cart', function() {
        productPage.addItemToCart();
        // Expects 'Product successfully added to your shopping cart' to be shown in the Pop Up Cart 
        expect(productPage.cartPopupText()).toContain('Product successfully added to your shopping cart');
    })
});